let camera, scene, renderer, cube;
let upload = document.getElementById('upload')

function init() {
	// Init scene
	scene = new THREE.Scene();

	// Init camera (PerspectiveCamera)
	camera = new THREE.PerspectiveCamera(
		75,
		window.innerWidth / window.innerHeight,
		0.1,
		1000
	);

	// Init renderer
	renderer = new THREE.WebGLRenderer({ antialias: true });

	// Set size (whole window)
	renderer.setSize(window.innerWidth, window.innerHeight);

	// Render to canvas element
	document.body.appendChild(renderer.domElement);

	// Init BoxGeometry object (rectangular cuboid)
	const geometry = new THREE.BoxGeometry(2, 2, 2);

	// Create material with color
	const material = new THREE.MeshBasicMaterial({ color: 0x0000ff });

	// Add texture - 
	// const texture = new THREE.TextureLoader().load('textures/crate.gif');

	// Create material with texture
	// const material = new THREE.MeshBasicMaterial({ map: texture });

	// Create mesh with geo and material
	cube = new THREE.Mesh(geometry, material);
	// Add to scene
	scene.add(cube);

	// Position camera
    camera.position.z = 5;
    
    var objects = [];
    objects.push(cube)
    var controls = new THREE.DragControls( objects, camera, renderer.domElement );
}

// Draw the scene every time the screen is refreshed
function animate() {
	requestAnimationFrame(animate);

	// Rotate cube (Change values to change speed)
	// cube.rotation.x += 0.01;
	// cube.rotation.y += 0.01;

	renderer.render(scene, camera);
}

function onWindowResize() {
	// Camera frustum aspect ratio
	camera.aspect = window.innerWidth / window.innerHeight;
	// After making changes to aspect
	camera.updateProjectionMatrix();
	// Reset size
	renderer.setSize(window.innerWidth, window.innerHeight);
}

function uploadObject() {
	console.log(this)
	var loader = new FBXLoader();
	loader.load()
}

function download(filename, text) {
	var element = document.createElement('a');
	element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
	element.setAttribute('download', filename);
  
	element.style.display = 'none';
	document.body.appendChild(element);
  
	element.click();
  
	document.body.removeChild(element);
}

// Instantiate a exporter
var exporter = new THREE.GLTFExporter();

document.getElementById( 'export-object' ).addEventListener( 'click', function () {

	var options = {};
	// Parse the input and generate the glTF output
	exporter.parse( scene, function ( gltf ) {
		var myJSON = JSON.stringify(gltf);
		console.log(myJSON);
		// var blob = new Blob([myJSON], {type: "text/plain;charset=utf-8"});
		download("scene.glb", myJSON)
	}, options );

} );

window.addEventListener('resize', onWindowResize, false);
upload.addEventListener('change', uploadObject);

init();
animate();
